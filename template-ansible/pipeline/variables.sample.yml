#
# Cloud Provider
#

# Cloud credentials to build the Ansible dynamic inventory.

#. aws_access_key (required): ((aws.access_key))
#+ Amazon AWS access key to build the Ansible dynamic inventory. See value format [here](https://docs.cycloid.io/advanced-guide/integrate-and-use-cycloid-credentials-manager.html#vault-in-the-pipeline)
aws_access_key: ((aws.access_key))

#. aws_secret_key (required): ((aws.secret_key))
#+ Amazon AWS secret key to build the Ansible dynamic inventory. See value format [here](https://docs.cycloid.io/advanced-guide/integrate-and-use-cycloid-credentials-manager.html#vault-in-the-pipeline)
aws_secret_key: ((aws.secret_key))

#. aws_default_region (required): eu-west-1
#+ Amazon AWS region to build the Ansible dynamic inventory.
aws_region: eu-west-1

#. azure_subscription_id (required): ((azure.subscription_id))
#+ Azure subscription ID to build the Ansible dynamic inventory.
azure_subscription_id: ((azure.subscription_id))

#. azure_tenant_id (required): ((azure.tenant_id))
#+ Azure tenant ID to build the Ansible dynamic inventory.
azure_tenant_id: ((azure.tenant_id))

#. azure_client_id (required): ((azure.client_id))
#+ Azure client ID to build the Ansible dynamic inventory.
azure_client_id: ((azure.client_id))

#. azure_client_secret (required): ((azure.client_secret))
#+ Azure client secret to build the Ansible dynamic inventory.
azure_client_secret: ((azure.client_secret))

#. gcp_credentials_json (required): ((gcp.json_key))
#+ Google Cloud Platform JSON credentials to build the Ansible dynamic inventory. See value format [here](https://docs.cycloid.io/advanced-guide/integrate-and-use-cycloid-credentials-manager.html#vault-in-the-pipeline)
gcp_credentials_json: ((gcp.json_key))


#
# Inventory Targeting
#

#. host_targeting_mode (required): ""
#+ Select how to target specific hosts. You can target a set of VMs on supported cloud providers using their dynamic inventory (based on tags matching), or target one VM using Cycloid Asset Inventory, or build your own list of hosts (FQDN or IP addresses)
host_targeting_mode: ""

#. cai_host (required): ''
#+ The targeted host IP addres or FQDN from the Cycloid Asset Inventory (cai).
cai_host: ""

#. hosts_inventory (required): "{'project':'($ project $)','env':'($ environment $)'}"
#+ An Ansible Hosts pattern that can refer to a single host, an IP address, an inventory group, a set of groups, or all hosts in your inventory. If you enabled a dynamic inventory above, groups have been created from Hosts tags. For example, to target all hosts tagged with role=fe AND env=prod, you would enter here: tag_role_fe:&tag_env_prod
hosts_inventory: ""


#
# Hosts Connectivity
#

#. hosts_username (required): admin
#+ The username to connect to via SSH to execute the Ansible playbook.
hosts_username: admin

#. hosts_private_key (required): ((keypair.ssh_prv))
#+ The private SSH key to connect via SSH to execute the Ansible playbook. The Cycloid workers shall be able to connect to targeted hosts via a public IP address.
hosts_private_key: ((keypair.ssh_prv))


#
# Operations
#

#. hosts_reboot (required): false
#+ Reboot all the targeted hosts.
hosts_reboot: false

#. hosts_service (required): ""
#+ A single service to restart on a Linux machine. Supported init systems include BSD init, OpenRC, SysV, Solaris SMF, systemd, upstart.
hosts_service: ""

#. hosts_package (required): ""
#+ A single package to install or upgrade on a Debian or Ubuntu OS.
hosts_package: ""

#. hosts_user (required): ""
#+ A single user to create.
hosts_user: ""


#
# Stack Git repository
#

#. stack_git_repository (required): ($ scs_url $)
#+ Git repository URL containing the stack.
stack_git_repository: ($ scs_url $)

#. stack_git_branch (required): ($ scs_branch $)
#+ Branch to use on the stack Git repository.
stack_git_branch: ($ scs_branch $)
{% if scs_cred_type == "ssh" %}
#. stack_git_private_key (required): ((($ scs_cred_path $).ssh_key))
#+ SSH key pair to fetch the stack Git repository.
stack_git_private_key: ((($ scs_cred_path $).ssh_key))
{% else %}
#. stack_git_username (required): ((($ scs_cred_path $).username))
#+ Username for HTTP(S) auth when pulling/pushing.
stack_git_username: ((($ scs_cred_path $).username))

#. stack_git_token (required): ((($ scs_cred_path $).password))
#+ Password for HTTP(S) auth when pulling/pushing.
stack_git_token: ((($ scs_cred_path $).password))
{% endif %}
#. stack_ansible_path (required): ($ stack_path $)/ansible
#+ Path of Ansible files in the stack git repository
stack_ansible_path: ($ stack_path $)/ansible


#
# Config Git repository
#

#. config_git_repository (required): ($ cr_url $)
#+ Git repository URL containing the config of the stack.
config_git_repository: ($ cr_url $)

#. config_git_branch (required): ($ cr_branch $)
#+ Branch to use on the config Git repository.
config_git_branch: ($ cr_branch $)
{% if cr_default_cred_type == "ssh" %}
#. config_git_private_key (required): ((($ cr_cred_path $).ssh_key))
#+ SSH key pair to fetch the config Git repository.
config_git_private_key: ((($ cr_cred_path $).ssh_key))
{% else %}
#. config_git_username (required): ((($ cr_cred_path $).username))
#+ Username for HTTP(S) auth when pulling/pushing.
config_git_username: ((($ cr_cred_path $).username))

#. config_git_token (required): ((($ cr_cred_path $).password))
#+ Password for HTTP(S) auth when pulling/pushing.
config_git_token: ((($ cr_cred_path $).password))
{% endif %}
#. config_ansible_path (required): ($ project $)/ansible/environments
#+ Path of Ansible files in the config git repository
config_ansible_path: ($ project $)/ansible/environments


#
# Default variables that shouldn't be changed except if needed
#

#. env (required): ($ environment $)
#+ Name of the project's environment.
env: ($ environment $)

#. project (required): ($ project $)
#+ Name of the project.
project: ($ project $)

#. customer (required): ($ organization_canonical $)
#+ Name of the Cycloid Organization, used as customer variable name.
customer: ($ organization_canonical $)

#. cycloid_api_url (required): https://http-api.cycloid.io
#+ Cycloid API URL.
cycloid_api_url: ($ api_url $)

#. cycloid-toolkit ansible version (required): 'v2.9'
#+ cycloid-toolkit docker image tag to use (https://hub.docker.com/r/cycloid/cycloid-toolkit/tags).
ansible_version: 'v2.9'